Sincronización de calendarios
=============================

1. Instalación de Lightning
---------------------------

Para poder realizar a sincronización dos calendarios contra o servidor de DavMail hai que instalar o complemento Lightning en Thunderbird.

https://addons.mozilla.org/es/thunderbird/addon/lightning/

2. Crear novo calendario
------------------------

Unha vez instalado Lightning, xa podemos acceder ao apartado de calendarios e usar a entrada de menú **Novo calendario...**. E escollemos a opción **Na rede**.

.. image:: ./images/davmail01.png
   :align: center

No apartado de localización do calendario, escollemos a opción de **CalDAV**, e escribimos a seguinte URL:

https://davmail.concello.santiagodecompostela.org/users/$EMAIL/calendar

substituíndo a variable ``$EMAIL`` polo enderezo de correo completo da conta coa que sincronizar o calendario.

.. image:: ./images/davmail02.png
   :align: center

E finalmente, escollemos un nome descriptivo, e unha cor personalizada para este calendario. Isto vai axudar a diferencialo doutros calendarios sincronizados.

.. image:: ./images/davmail03.png
   :align: center

3. Datos de autenticación
-------------------------

.. image:: ./images/davmail04.png
   :align: center

Se aparece unha xanela solicitando as credenciais para o inicio de sesión, hai que modificar o **Nome de usuario**. En lugar do enderezo de correo, hai que introducir o seguinte texto::

  CONCELLO\{username}

[UPDATE]. Neste campo debemos introducir o nome do usuario (sen o resto do enderezo de correo). O caso anterior foi causado pola introdución de varias credenciais en Thunderbird.

4. Configurar calendario compartido
-----------------------------------

Para poder configurar un calendario compartido doutro usuario, seguimos os mesmos pasos que na creación do calendario do usuario. Tan só temos que modificar o enderezo do calendario, e inserir o correo electrónico do 'outro' usuario. As credenciais son as do usuario principal.

Comprobar se existen varias credenciais dadas de alta para ese calendario desde ``Preferencias->Seguranza->Contrasinais->Contrasinais gardados…``.
