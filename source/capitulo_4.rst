Configuración de correo electrónico
===================================

Instrucións para configurar unha conta de correo electrónico en Mozilla Thunderbird.

1. Datos globais
----------------

* Servidor Correo: **scq1e2**
* IMAP port: **143**

  * Seguridade da conexión: **Ningunha**
  * Método de autenticación: **NTLM**
  * Nome de usuario: **username**

* SMTP port: **25**

  * Seguridade da conexión: **Ningunha**
  * Método de autenticación: **ontrasinal, transmitido de forma insegura**
  * Nome de usuario: **username**


2. Crear nova conta de correo electrónico
-----------------------------------------

Iniciamos o proceso de ``Configuración de conta de correo`` a través dalgunha das opcións dispoñibles (menú, asistente, ...).

.. image:: ./images/email_01.png
   :align: center

Unha vez cubertos os datos relativos á conta do usuario, accedemos á configuración manual.

.. image:: ./images/email_02.png
   :align: center

Unha vez introducido o nome do servidor ``scq1e2`` facemos clic en **Volver a probar** para que detecte a configuración. Só hai que modificar o campo ``Autenticación`` de **SMTP** para que se configure co valor ``Contrasinal normal``.

Vai aparecer unha alerta de seguridade que debemos aceptar facendo clic na caixa de verificación **Entendo os riscos**.

.. image:: ./images/email_03.png
   :align: center
