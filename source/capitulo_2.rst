Montaxe de directorios compartidos
======================================

1. Introdución
--------------

Para poder realizar a montaxe dos directorios compartidos a través de SAMBA, existe un script personalizado que se executa durante o inicio de sesión de usuario. O script está localizado en ``/var/lib/pam-script/auth.d/99_remote_resources_mount``:

:download:`Script v1.0 <files/99_remote_resources_mount_orixinal>`

**Problemas detectados**

Un dos problemas detectados é que se montan máis unidades das que o usuario vai utilizar. Hai varios directorios que aparecen montados, aínda que despois non hai permisos suficientes para poder acceder ao contido, ben sexa de lectura e/ou de escritura.

Para poder evitar este comportamento, realizase unha modificación no script, así como o engadido doutro script con capacidade gráfica para poder activar ou desactivar os directorios que se deben montar finalmente.

2. Script de configuración de directorios compartidos
-----------------------------------------------------

O seguinte script crea unha base de datos coa listaxe de todos aqueles directorios que se lle permite montar ao usuario, e que a partir da primeira execución vai poder seleccionar para que se monten de novo ou non.
É importante destacar que a orixe de datos para este script é un directorio onde aparecen todos aqueles subdirectorios creados durante o primeiro inicio de sesión. E a partir deses datos iniciais imos poder seleccionar as seguintes montaxes.

:download:`Script v2.0 <files/99_remote_resources_mount>`

::

    if [[ -f /home/$PAM_USER/mount.db ]]; then
        if grep -F /$share /home/$PAM_USER/mount.db | grep -q FALSE; then
            send_to_log "Non se monta $share"
            continue;
        fi
    fi

3. Xestión da base de datos
---------------------------

Para poder realizar a selección de directorios que se van a montar, hai que crear a base de datos, que estará almacenada na seguinte ruta: ``~/mount.db``, e que terá o seguinte formato::

    TRUE /home/user/Compartidos/publico
    FALSE /home/user/Compartidos/privado
    [...]

Este ficheiro (base de datos) é creado e mantido a través do seguinte script:

:download:`Xestion.sh <files/xestion.sh>`

.. image:: ./images/xestion.png
   :align: center


