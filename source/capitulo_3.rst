Conexión remota
==================

1 Configuración XRDP
----------------------

Para poder realizar conexións remotas aos equipos con Debian, é necesario realizar as seguintes instalacións::

  apt install -y xrdp tigervnc-standalone-server tigervnc-common

e a continuación, modificamos o ficheiro ``/etc/xrdp/xrdp.ini`` co seguinte contido::

  [...]
  
  [Xnvc]
  name=Xvnc

  [...]

  [Xorg]
  name=Xorg

  [...]

O único que hai que facer respecto do ficheiro orixinal, é mover a entrada relativa a ``Xorg`` ao segundo lugar, deixando no primeiro a relativa a ``Xvnc``.

Con esta configuración no equipo local é posible iniciar unha nova sesión gráfica cun usuario diferente ao que poida estar conectado localmente.

2 Configuración VNC
---------------------

Para poder iniciar un servidor VNC hai que iniciar sesión e executar unha entrada no menú de aplicativos. Vaise facer uso de ``x11vnc``, para que se poidan abrir conexións remotas a través dun visor VNC desde calquera sistema operativo. Unha vez lanzado este proceso, quedará dispoñible ata que se peche a sesión por parte do usuario conectado localmente.

Ficheiro ``/usr/local/bin/vncserver.sh`` que abre ``x11vnc`` en modo background para aceptar conexións remotas::

  #!/bin/bash
  x11vnc --auth /run/user/$(id -u)/gdm/Xauthority --forever --bg -o /home/$USER/.x11vnc.log --display :1

:download:`vncserver.sh<files/vncserver.sh>`

**NOTA**: Hai que comprobar que unha vez copiado o ficheiro ``vncserver.sh`` teña activado o permiso de execución.

Ficheiro ``/usr/share/applications/gnome-x11vnc.desktop`` que engade unha entrada no menú de aplicativos::

  [Desktop Entry]
  Name=VNCServer
  Comment=Conexión remota
  Keywords=vnc,remote
  Exec=/usr/local/bin/vncserver.sh
  Icon=computer
  Terminal=false
  Type=Application
  StartupNotify=false
  Categories=GNOME;GTK;Network;RemoteAccess

:download:`gnome-x11vnc.desktop<files/gnome-x11vnc.desktop>`

Para poder establecer unha conexión a través de VNC é necesario que o usuario execute este proceso.

Se queremos comprobar que o servidor VNC está en execución, podemos facelo a través do seguinte comando::

  netstat -tulpen

e comprobamos que existan procesos asociados ao porto ``5900`` ou seguintes.

Se temos que volver a executar o servizo, ou se activamos unha nova interface de rede, debemos pechar os procesos anteriores::

  killall x11vnc
