Migración a GNU/Linux
==========================================

.. toctree::
   :maxdepth: 2

   capitulo_1
   capitulo_2
   capitulo_3
   capitulo_4
   capitulo_5
   capitulo_6

Licencia
--------

.. toctree::
   :maxdepth: 1

   licencia
