Instalación de imaxe DebianSCQ
==============================

1. Descarga de imaxe ISO
------------------------

O ficheiro .ISO ``santiago_othar_64_stretch.iso`` proporcionado polo provedor pode descargase desde a seguinte URL:

https://drive.google.com/open?id=0BxXm1JEWaocgc3NHbGxPNThSZzg


2. Arranque de equipo con USB arrancable
----------------------------------------

Para poder crear un USB arrancable, executamos o seguinte comando::

  dd if=/ruta/santiago_othar_64_stretch.iso of=/dev/sdX ; sync

**NOTA**: *Detectouse que nos equipos de sobremesa empregados é necesario activar a interface de rede na BIOS.*

3. Execución de ferramenta de clonado
-------------------------------------

Unha vez que iniciamos un equipo desde este USB aparece unha sesión de bash, na que executamos os seguintes comandos::

  export efi=yes
  export parting=yes
  othar -f

A opción ``export parting=yes`` só será necesaria cando o disco do equipo non teña táboa de particións. Se xa tivo unha instalación, vaise conservar o directorio ``/home`` e ``/puppet``. Deste xeito, vaise manter o certificado que está aprobado no servidor.

Se nos pide un ``hostname``, escribimos o nome que aparece na etiqueta do equipo.

Este proceso executa a ferramenta ``partclone``, e fai un copiado rápido de ficheiros. Unha vez finalizado, reiniciase o equipo.

4. Configuración de parámetros iniciais
---------------------------------------

4.1 Rede local
~~~~~~~~~~~~~~

Chegados a este punto, debemos comprobar se temos configurada a rede::

  ip a

En caso de non ter configurado enderezo IP na interface de rede (p.ex enp2s0f2), debemos executar o seguinte comando::

  dhclient enp2s0f2

E, para que sexa permanente engadimos a seguinte liña no ficheiro ``/etc/network/interfaces``::

  [...]
  auto enp2s0f2
  iface enp2s0f2 inet dhcp

4.2 Hostname
~~~~~~~~~~~~

No ficheiro ``/etc/hosts`` temos que modificar a entrada para que o nome do equipo apareza co seguinte formato::

  scqxxxxpc.concello.santiagodecompostela.org

5. Execución de puppet
----------------------

Agora, xa podemos usar o seguinte comando::

  puppet agent -t

Se aparece unha mensaxe de erro do tipo ``500 error - non response``, executamos a orde anterior coa opción seguinte::

  puppet agent -t --no-stringify_facts

**NOTA**: *Hai que solicitar permiso ao provedor para que se poida acceder e descargar os ficheiros necesarios de Puppet.*

Este proceso pode levar máis dunha hora, e aparecerán multitude de mensaxes por pantalla. Fixarse se sae algún de cor vermella. Os normais serán brancos ou verdes.

6. Resolución de erros
----------------------

Se con este último paso non é suficiente para reiniciar a restauración, executaremos o seguinte comando::

  apt-get update && apt-get install -y smartmontools puppet && killall sleep

