Amosar enderezo IP no escritorio
================================

1. Instalación en Cinnamon
--------------------------

Para poder amosar o enderezo IP no panel inferior de Cinnamon hai que instalar o seguinte applet:

https://github.com/appdevsw/commandrunner

Unha vez instalado e engadido ao panel hai que configuralo para que execute a seguinte orde::

  hostname -I

co prefixo::

  IP:_ 


2. Instalación en Gnome3
------------------------

En GnomeShell cabe a posibilidade de instalar unha extensión chamada ShowIP:

https://extensions.gnome.org/extension/941/show-ip/
https://extensions.gnome.org/extension/1161/show-ip/

Calquera das dúas opcións é valida. A primeira amosa sempre o enderezo IP na barra superior, e a segunda hai que facer clic no menu superior dereita.
Probaronse as dúas extensións con éxito nun equipo de probas.

